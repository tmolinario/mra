package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	//User Story 1
	public void test1() throws MarsRoverException {

		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(2,2)");
		listObstacle.add("(1,2)");
		
		MarsRover rover = new MarsRover(10, 10, listObstacle);
		
		boolean test = rover.planetContainsObstacleAt(2,2);
		assert true == test;
		
		test = rover.planetContainsObstacleAt(1,2);
		assert true == test;
		
	}
	
	
	@Test
	//User Story 2
	public void test2() throws MarsRoverException {

		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(2,2)");
		listObstacle.add("(1,2)");
		
		MarsRover rover = new MarsRover(10, 10, listObstacle);
		String s = "(0,0,N)";
		
		assertEquals(s, rover.executeCommand(""));
		
	}
	
	@Test
	//User Story 3
	public void test3() throws MarsRoverException {

		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(2,2)");
		listObstacle.add("(1,2)");
		
		MarsRover rover = new MarsRover(10, 10, listObstacle);
		
		assertEquals("(0,0,E)", rover.executeCommand("r"));
		assertEquals("(0,0,N)", rover.executeCommand("l"));
		
	}
	
	@Test
	//User Story 4
	public void test4() throws MarsRoverException {

		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(2,2)");
		listObstacle.add("(1,2)");
		
		MarsRover rover = new MarsRover(10, 10, listObstacle);
		String s1 = "(0,1,N)";
		
		assertEquals(s1, rover.executeCommand("f"));
		
	}
	
	@Test
	//User Story 5
	public void test5() throws MarsRoverException {

		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(2,2)");
		listObstacle.add("(1,2)");
		
		MarsRover rover = new MarsRover(10, 10, listObstacle);
		String s1 = "(0,0,N)";
		
		rover.executeCommand("f");
		
		assertEquals(s1, rover.executeCommand("b"));
		
	}
	
	@Test
	//User Story 6
	public void test6() throws MarsRoverException {

		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(9,9)");
		
		MarsRover rover = new MarsRover(10, 10, listObstacle);
		String s1 = "(2,2,E)";
		
		assertEquals(s1, rover.executeCommand("ffrff"));
		
	}
	
	@Test
	//User Story 7
	public void test7() throws MarsRoverException {

		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(2,2)");
		
		MarsRover rover = new MarsRover(10, 10, listObstacle);
		
		assertEquals("(0,8,N)", rover.executeCommand("bb"));
		
	}
	
	@Test
	//User Story 8
	public void test8() throws MarsRoverException {

		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(2,2)");
		
		MarsRover rover = new MarsRover(10, 10, listObstacle);
		
		assertEquals("(1,2,E)(2,2)", rover.executeCommand("ffrfff"));
		
	}
	
	@Test
	//User Story 9
	public void test9() throws MarsRoverException {

		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(2,2)");
		listObstacle.add("(2,1)");
		
		MarsRover rover = new MarsRover(10, 10, listObstacle);
		
		assertEquals("(1,1,E)(2,2)(2,1)", rover.executeCommand("ffrfffrflf"));
		
	}
	
	@Test
	//User Story 10
	public void test10() throws MarsRoverException {

		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(0,9)");
		
		MarsRover rover = new MarsRover(10, 10, listObstacle);
		
		assertEquals("(0,0,N)(0,9)", rover.executeCommand("b"));
		
	}
	
	
	@Test
	//User Story 11
	public void test11() throws MarsRoverException {

		List<String> listObstacle = new ArrayList<String>();
		listObstacle.add("(5,0)");
		listObstacle.add("(0,5)");
		listObstacle.add("(2,2)");
		
		MarsRover rover = new MarsRover(6, 6, listObstacle);
		
		assertEquals("(0,0,N)(2,2)(0,5)(5,0)", rover.executeCommand("ffrfffrbbblllfrfrbbl"));
		
	}

}
